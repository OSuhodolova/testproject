﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestProject.Models;

namespace TestProject.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            using (var context = new TestProjectDbContext())
            {
                var areaListItems = new List<SelectListItem> { new SelectListItem { } };
                var areas = context.AddressAreas.ToDictionary(x => x.Id, y => y.Name);                                
                areaListItems.AddRange(areas.Select(x => new SelectListItem { Value = x.Key.ToString(), Text = x.Value }));
                ViewBag.AreaSelect = areaListItems;

                var workScopeListItems = new List<SelectListItem> { new SelectListItem { } };
                var workScopes = context.WorkScopes.ToDictionary(x => x.Id, y => y.Name);
                workScopeListItems.AddRange(workScopes.Select(x => new SelectListItem { Value = x.Key.ToString(), Text = x.Value }));
                ViewBag.WorkScopeSelect = workScopeListItems;
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(ProducerModel producer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    producer.AddToDatabase();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message;
                }
            }
            else
            {
                ViewBag.Error = "Некорректные данные";
            }

            if (!string.IsNullOrWhiteSpace(ViewBag.Error))
            {
                return View("SubmitFailed");
            }

            return View("SubmitOK");
        }

        public JsonResult GetCities(int areaId)
        {
            using (var context = new TestProjectDbContext())
            {
                var cities = context.AddressCities.Where(x=>x.AreaId == areaId).Select(x=> new { Id = x.Id, Name = x.Name}).ToList();
                return Json(cities);
            }
        }

        public JsonResult ServerValidation(ProducerModel producer)
        {
            if (!ModelState.IsValid)
            {
                var result = new Dictionary<string, string>();

                foreach (var fieldState in ModelState)
                {
                    if (fieldState.Value.Errors.Count > 0)
                    {
                        result.Add(fieldState.Key, fieldState.Value.Errors.Select(x => x.ErrorMessage).FirstOrDefault());
                    }
                }

                return Json(result);
            }

            return null;
        }
    }
}