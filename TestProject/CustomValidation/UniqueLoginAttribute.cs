﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TestProject.Models;

namespace TestProject.CustomValidation
{
    public class UniqueLoginAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ProducerModel producer = (ProducerModel)validationContext.ObjectInstance;

            using (var context = new TestProjectDbContext()) {
                if(context.Producers.Any(x=>x.Login == producer.Login)) {
                    return new ValidationResult(ErrorMessage);
                }
            }

            return ValidationResult.Success;
        }
    }
}