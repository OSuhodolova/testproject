﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestProject.Models;

namespace TestProject
{
    public class TestProjectDbContext : DbContext
    {
        public TestProjectDbContext() : base("baseConnection")
        { }

        public DbSet<AddressAreaModel> AddressAreas { get; set; }
        public DbSet<AddressCityModel> AddressCities { get; set; }
        public DbSet<WorkScopeModel> WorkScopes { get; set; }
        public DbSet<ProducerModel> Producers { get; set; }
    }
}