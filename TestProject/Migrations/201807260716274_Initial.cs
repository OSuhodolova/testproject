namespace TestProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AddressAreaModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AddressCityModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AreaId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AddressAreaModels", t => t.AreaId, cascadeDelete: false)
                .Index(t => t.AreaId);
            
            CreateTable(
                "dbo.ProducerModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        AreaId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                        SiteURL = c.String(),
                        SpecifiedStoreDomain = c.Boolean(nullable: false),
                        StoreURL = c.String(),
                        EMail = c.String(nullable: false),
                        Login = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AddressAreaModels", t => t.AreaId, cascadeDelete: false)
                .ForeignKey("dbo.AddressCityModels", t => t.CityId, cascadeDelete: false)
                .Index(t => t.AreaId)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.WorkScopeModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkScopeModelProducerModels",
                c => new
                    {
                        WorkScopeModel_Id = c.Int(nullable: false),
                        ProducerModel_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkScopeModel_Id, t.ProducerModel_Id })
                .ForeignKey("dbo.WorkScopeModels", t => t.WorkScopeModel_Id, cascadeDelete: true)
                .ForeignKey("dbo.ProducerModels", t => t.ProducerModel_Id, cascadeDelete: true)
                .Index(t => t.WorkScopeModel_Id)
                .Index(t => t.ProducerModel_Id);

            InitAreas();
            InitCities();
            InitWorkScopes();
        }

        private void InitAreas()
        {
            Sql("SET IDENTITY_INSERT dbo.AddressAreaModels ON;");
            Sql("INSERT INTO dbo.AddressAreaModels (Id, Name) VALUES " +
                "(1, N'��������� ����')," +
                "(2, N'�������� �������')," +
                "(3, N'��������� �������')," +
                "(4, N'������������ �������');");
            Sql("SET IDENTITY_INSERT dbo.AddressAreaModels OFF;");
        }

        private void InitCities()
        {
            Sql("INSERT INTO dbo.AddressCityModels (AreaId, Name) VALUES " +
                "(1, N'�������')," +
                "(1, N'�����')," +
                "(2, N'������')," +
                "(2, N'�������')," +
                "(3, N'������')," +
                "(3, N'�������')," +
                "(4, N'������������')," +
                "(4, N'�����')," +
                "(4, N'���');");
        }

        private void InitWorkScopes()
        {
            Sql("INSERT INTO dbo.WorkScopeModels (Name) VALUES " +
                "(N'������')," +
                "(N'�����')," +
                "(N'������ ��� ������ � ������')," +
                "(N'������� �������')," +
                "(N'��������, �����, ���������')," +
                "(N'�� ��� ����')," +
                "(N'����- � ����������, ��������')," +
                "(N'��������, �������')," +
                "(N'������, ��������');");
        }

        public override void Down()
        {
            DropForeignKey("dbo.WorkScopeModelProducerModels", "ProducerModel_Id", "dbo.ProducerModels");
            DropForeignKey("dbo.WorkScopeModelProducerModels", "WorkScopeModel_Id", "dbo.WorkScopeModels");
            DropForeignKey("dbo.ProducerModels", "CityId", "dbo.AddressCityModels");
            DropForeignKey("dbo.ProducerModels", "AreaId", "dbo.AddressAreaModels");
            DropForeignKey("dbo.AddressCityModels", "AreaId", "dbo.AddressAreaModels");
            DropIndex("dbo.WorkScopeModelProducerModels", new[] { "ProducerModel_Id" });
            DropIndex("dbo.WorkScopeModelProducerModels", new[] { "WorkScopeModel_Id" });
            DropIndex("dbo.ProducerModels", new[] { "CityId" });
            DropIndex("dbo.ProducerModels", new[] { "AreaId" });
            DropIndex("dbo.AddressCityModels", new[] { "AreaId" });
            DropTable("dbo.WorkScopeModelProducerModels");
            DropTable("dbo.WorkScopeModels");
            DropTable("dbo.ProducerModels");
            DropTable("dbo.AddressCityModels");
            DropTable("dbo.AddressAreaModels");
        }
    }
}
