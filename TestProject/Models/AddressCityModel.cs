﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models
{
    public class AddressCityModel: BaseModel
    {
        public int AreaId { get; set; }
        public string Name { get; set; }

        public AddressAreaModel Area { get; set; }
    }
}