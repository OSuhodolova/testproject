﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TestProject.CustomValidation;

namespace TestProject.Models
{
    public class ProducerModel : BaseModel
    {
        [Required]
        [Display(Name = "Название компании и/или бренда")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Регион")]
        public int AreaId { get; set; }

        [Required]
        [Display(Name = "Город")]
        public int CityId { get; set; }

        [Required]
        [Display(Name = "Сфера деятельности")]
        public List<int> WorkScopeIds { get; set; }

        [Display(Name = "Адрес вашего сайта")]
        [Url(ErrorMessage = "Некорректный URL")]
        public string SiteURL { get; set; }

        [Display(Name = "Интернет-магазин расположен на отдельном домене")]
        public bool SpecifiedStoreDomain { get; set; }

        [Display(Name = "Адрес интернет-магазина")]
        [Url(ErrorMessage = "Некорректный URL")]
        public string StoreURL { get; set; }

        [Required]
        [Display(Name = "Электронный адрес")]
        [EmailAddress(ErrorMessage = "Некорректный Email")]
        public string EMail { get; set; }

        [Required]
        [Display(Name = "Логин")]
        [UniqueLogin(ErrorMessage = "Логин не уникален")]
        public string Login { get; set; }

        [Required]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        public AddressAreaModel Area { get; set; }
        public AddressCityModel City { get; set; }
        public List<WorkScopeModel> WorkScope { get; set; }

        //Возможно, более сложную логику стоило бы вынести в отдельный класс
        public void AddToDatabase()
        {
            using (var context = new TestProjectDbContext())
            {
                SetPasswordHash();
                SetWorkingScopes(context);

                context.Producers.Add(this);
                context.SaveChanges();
            }
        }

        private void SetPasswordHash()
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] bytes = Encoding.ASCII.GetBytes(Password);
                byte[] hash = md5.ComputeHash(bytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }

                Password = sb.ToString().ToUpper();
            }
        }

        private void SetWorkingScopes(TestProjectDbContext context)
        {
            var workScopes = context.WorkScopes.ToDictionary(x=>x.Id, y=>y);

            WorkScope = new List<WorkScopeModel>();
            foreach (var id in WorkScopeIds)
            {
                if (workScopes.ContainsKey(id))
                {
                    WorkScope.Add(workScopes[id]);
                }
            }
        }
    }
}