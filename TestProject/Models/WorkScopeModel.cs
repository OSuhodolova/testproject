﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models
{
    public class WorkScopeModel : BaseModel
    {
        public string Name { get; set; }
        public List<ProducerModel> Producers { get; set; }
}
}