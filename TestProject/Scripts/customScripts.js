﻿var errorMessageForRequired = "Не заполнено поле";

$(document).ready(function () {
    $("[data-val-required]").attr("data-val-required", errorMessageForRequired);
    $("[data-val-required]").prev("label").addClass("required-field-label");

    $("#AreaId").chosen();
    $("#CityId").chosen();
    $("#WorkScopeIds").chosen();
    $('#Password').password();
    $('#Password').showPassword();

    $("#AreaId").change(LoadCities);
    $("[data-val-required]").keyup(ToggleSubmitButton);
    $("[data-val-required]").change(ToggleSubmitButton);
    $("#SpecifiedStoreDomain").click(ToggleStoreUrlField);    
    
    ToggleSubmitButton();
    ToggleStoreUrlField();
    CustomizeValidation();
});

function LoadCities()
{
    $("#CityId").html("<option></option>")

    $.post({
        url: "/Home/GetCities",
        data: "areaId=" + $("#AreaId").val(),
        success: function (cities) {
            var optionsHtml = "<option></option>";

            if (cities.length > 0) {
                cities.forEach(function (item) {
                    optionsHtml += "<option value='" + item.Id + "'>" + item.Name + "</option>";
                });
            }

            $("#CityId").html(optionsHtml);
            $("#CityId").removeAttr("disabled");
            $("#CityId").chosen("destroy");
            $("#CityId").chosen();
        },
        error: function () {
            $("#CityId").attr("disabled");
        }
    });
}

function ToggleSubmitButton()
{
    var hasEmptyValue = false;
    $("[data-val-required]").each(function () {
        if ($.trim($(this).val()) == "") {
            hasEmptyValue = true;
        }
    });

    if (hasEmptyValue) {
        $("button[type='submit']").attr("disabled", "true");
    }
    else {
        $("button[type='submit']").removeAttr("disabled");
    }
}

function ToggleStoreUrlField()
{
    var hiddenParent = $("#StoreURL").closest(".form-group");

    if ($("#SpecifiedStoreDomain").is(":checked")) {
        hiddenParent.removeClass("hidden");
    }
    else {
        hiddenParent.addClass("hidden");
        $("#StoreURL").val("");
    }
}

function CustomizeValidation() {

    var form = $("form");

    $.validator.setDefaults({
        ignore: []
    });

    form.submit(serverValidation);
    var validatedOnServer = false;

    function serverValidation() {
        if (form.valid() && !validatedOnServer) {

            $.post({
                url: "/Home/ServerValidation",
                data: form.serialize(),
                success: function (response) {
                    if (response.length == 0) {
                        validatedOnServer = true;
                        form.submit();
                    }
                    else {
                        form.validate().showErrors(response);
                    }
                }
            });

            return false;
        }
    }
}