﻿$.fn.extend({
    showPassword: function () {
        var button = $('<div class="pwd-trigger show-pwd" title="Отобразить/скрыть пароль"></div>');
        var $this = $(this);

        $this.parent().append(button);

        button.mousedown(onPasswordShow);
        button.mouseup(onPasswordHide);
        button.mouseout(onPasswordHide);

        function onPasswordShow() {
            if ($this.attr("type") == "password") {

                $this.attr("type", "text");
                button.removeClass("show-pwd");
                button.addClass("hide-pwd");
            }
        }

        function onPasswordHide() {
            if ($this.attr("type") == "text") {

                $this.attr("type", "password");
                button.addClass("show-pwd");
                button.removeClass("hide-pwd");
            }
        }
    }
});